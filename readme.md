# Projet Roborally

### Presentation
Ce projet vise à créer un joueur automatique (C++) pour le jeu [Roborally](https://fr.wikipedia.org/wiki/RoboRally). 
<br>
Le sujet est disponible en pdf [sur la page du cours](https://liris.cnrs.fr/vincent.nivoliers/lifap6/Supports/Projet/roborally.pdf).

### Groupe
* Constantin Magnin p1806593 <br>
* Como Adrien       p1709079 <br>

### Installation et execution
A la racine du projet faire un  : 
```
make clean ; make 
```
puis executer  : 
```
./app board.txt 
```
*ou tout autre board (big_board.txt, custom_board.txt..etc)*

### Avancée du projet
- [x] Structures de données (*graph*)
- [x] Plus court chemin dans un graphe (*dijkstra*)
- [x] Joueur artificiel (*custom made*)<br><br>
- [x] Valgrind 
    * Aucune fuite mémoire
- [x] Tests 
    * le programme ne crashe pas (même jamais)
    * peu importe les les fichiers boards en parametres, le graph est construit correctement
    * dijkstra fonctionne correctement et indique toujours le plus court chemin
    * l'algorithme du jeu marche à tous les coups peut importe les positions de départs et d'arrivés du robot <br> 
      et des coups possibles selon les cartes

### Le projet 
**Src contient** : 
-board.cpp/hpp  : contient le code pour créer un robot et jouer des coups. <br>
-test_board.cpp : contient la fonction principale permettant de tester les différentes fonctions. <br> 
-httplib.hpp    : jouer au jeu Roborally (voir localhost). <br>
-app.cpp        : fait office de main pour le jeu. <br><br>

**Les plateaux de jeu** : <br>
-big_board.txt  : configuration grand plateau. <br>
-all_titles.txt : toutes cases possible d'un plateau. <br>
-board.txt      : configuration plateau par default. <br><br>

**Custom class**    : <br>
-Graph.cpp/hpp  : classe creer pour le graph du plateau de chaque cases + implémentation dijkstra <br>
-Deck.cpp/hpp   : l'IA qui permet de chercher le plus court chemin en prenant en compte les cartes données aleatoirement et des regles du jeu

### Complexite (algorithmique / memoire)
generation graphe : /<br>
algo de dijkstra : /<br>
algo de deplacement du robot en fonction des cartes : / <br>
