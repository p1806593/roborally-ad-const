//
// Created by constantin on 06/04/2021.
//

#include "Deck.hpp"
#include <queue>

RandomManager::RandomManager(const uint8_t & _low, const uint8_t & _high) {
    std::random_device rD;
    randomEngine =std::default_random_engine(rD());
    manager = std::uniform_int_distribution<uint8_t>(_low,_high);
}

uint8_t RandomManager::getRandom() {
    return manager(randomEngine);
}

void Hand::drawHand() {

}

Hand::Hand(const unsigned int & _handSize) {
    randomManager = RandomManager(0,6);
    for (int i = 0; i < _handSize; ++i) {
        currentHand.emplace_back((Robot::Move)randomManager.getRandom());
    }
}

std::vector<Card> Hand::makePath(const Board& _board, Robot _robot, Graph& _graph, Location & _to) {
    std::vector<Card> handInstance= currentHand;
    std::vector<Card> res;
    std::vector<Card> dunnoYet;
    std::vector<std::pair<Robot::Move, Robot>> bestMoves = _graph.getBestMoves();



    std::cout<<std::endl<<"moves to make :"  << std::min((int)bestMoves.size(),5)<< std::endl<<_robot<< *this << std::endl;
    bool BMstillPossible = true;

    Robot instance = _robot;
    Robot instanceCopy = instance;

    for (int i = 0; i < std::min((int)bestMoves.size(),5); ++i) {
        std::vector<Card> bin;

        for (const auto &_status : _graph.allStatus) {
            Robot goToInstance= Robot(_to, _status);
            if(_robot == goToInstance){
                return res;
            }
        }

        int leastWeightAdd=std::numeric_limits<int>::infinity();
        Card currentBestCard=handInstance[0];
        bool BMFound = false;
        bool leadsToDeath = true;
        bool oneIsValid= false;
        int replaced=0;
        int ind=-1;
        //std::cout << "cards in hand : "<< handInstance.size()<<std::endl;
        for(Card cards :handInstance){

            ind++;
            if (BMFound){
                bin.push_back(cards);
                continue;
            }
            if (bestMoves[bestMoves.size()-i-1].first == cards.getAction() && BMstillPossible){
                res.push_back(cards); // this card in the next best move ! no verification needed
                //std::cout << " match ! " << i<< " " << bestMoves[bestMoves.size()-i-1].first << " : " << cards.getAction()<<" "<<ind<<std::endl;
                BMFound=true;
                _board.play(instance,cards.getAction());

                if(replaced!=0){
                    bin.push_back(currentBestCard);
                }
                currentBestCard= cards;
                leadsToDeath = false;
                oneIsValid = true;
                ++replaced;



                continue;
            }


            auto costMapInstance = _graph.getCostMap();
            Graph graphInstance = _graph;

            Robot currentDir(_to, Robot::Status::NORTH);
            for (const auto &_status : _graph.allStatus) {
                Robot goToInstance= Robot(_to, _status);
                if (costMapInstance[goToInstance] < costMapInstance[currentDir]) {
                    currentDir = goToInstance;
                }
            }

            //Not found in best moves :(
            //What if we play that card ?
            instanceCopy = instance;

            _board.play(instanceCopy,cards.getAction());
            graphInstance.findPath(instance,_to);
            //std::cout<<instanceCopy<< " with "<<cards<<std::endl;
            int weightToDir = graphInstance.getCostMap()[instance]; // Oh look ! a new cost !

            if(instanceCopy.status == Robot::Status::DEAD){
                bin.push_back(cards);
                leadsToDeath = true;
                //std::cout<<i<<" " << ind <<" this card kill the robot ! " << cards <<std::endl;

                continue;
            }
            else if (weightToDir <= leastWeightAdd){
                if(replaced!=0){
                    bin.push_back(currentBestCard);
                }
                currentBestCard= cards;

                leastWeightAdd = weightToDir;
                leadsToDeath = false;
                oneIsValid = true;
                ++replaced;
            } else {
                bin.push_back(cards);
            }

        }
        if (!BMFound && (oneIsValid||!leadsToDeath)) {
            //std::cout<< i << " card added in play : "<<currentBestCard<<" "<< ind<<std::endl;
            res.push_back(currentBestCard);
            instance = instanceCopy;
            BMstillPossible = false;

        }
        std::cout<<i<<" cards in bin : "<<bin.size() << std::endl;
        handInstance = bin;
    }


    return res;
}




