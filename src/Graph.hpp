#ifndef ROBORALLY_GRAPH_HPP
#define ROBORALLY_GRAPH_HPP

#include "board.hpp"

#include <vector>
#include <iostream>
#include <unordered_map>
#include <stack>

using namespace  RR;

struct RobotHash {
    std::size_t operator()(const Robot& r) const; //  actual hashing (thx teachers)
    bool operator()(const Robot& a, const Robot& b) const; // predicate to verify key
};


class Graph{
public:
    Graph(const Robot & _init, const Board & _board);
    void generateGraph(const Robot & _init, const Board & _board);
    friend std::ostream& operator<<(std::ostream& out, const Graph& graph);
    std::vector<std::pair<Robot::Move, Robot>> findPath(const Robot & _from, const Location & _to);
    std::vector<std::pair<Robot::Move, Robot>> getBestMoves() { return  bestMoves;}
    std::unordered_map<Robot, uint64_t, RobotHash, RobotHash> getCostMap(){return costMap;}

    const std::array<Robot::Status, 4> allStatus = { //all possible statuses
            Robot::Status::NORTH,
            Robot::Status::SOUTH,
            Robot::Status::EAST,
            Robot::Status::WEST
    };
    
    const std::array<Robot::Move, 7> moves = { //all possible directions
                    Robot::Move::FORWARD_1,
                    Robot::Move::FORWARD_2,
                    Robot::Move::FORWARD_3,
                    Robot::Move::BACKWARD_1,
                    Robot::Move::TURN_LEFT,
                    Robot::Move::TURN_RIGHT,
                    Robot::Move::U_TURN
            };
private :
    std::unordered_map<Robot, std::vector<std::pair<Robot::Move,Robot>>,RobotHash,RobotHash> graph;
    std::unordered_map<Robot, uint64_t, RobotHash, RobotHash> costMap;
    std::vector<std::pair<Robot::Move, Robot>> bestMoves;
};


#endif //ROBORALLY_GRAPH_HPP
