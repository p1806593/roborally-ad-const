#include "Graph.hpp"
#include <queue>
#include <stack>
#include <utility>
#include <unordered_set>
#include <thread>
#include <array>
#include <chrono>
#include <cstdint>

using namespace RR;


std::size_t RobotHash::operator()(const Robot &r) const {
    //create a bitset to pack line column and orientation
    std::bitset<16 * sizeof(int) + 8 * sizeof(Robot::Status)> concat;
    //pack the line
    concat |= r.location.line;
    concat <<= 8 * sizeof(int);
    //pack the colum,
    concat |= r.location.column;
    concat <<= 8 * sizeof(Robot::Status);
    //pack the orientation
    concat |= +(int) r.status;
    //hash the bitset using the standard implementation on bitsets
    return std::hash<std::bitset<16 * sizeof(int) + 8 * sizeof(Robot::Status)> >()(concat);
}

bool RobotHash::operator()(const Robot &a, const Robot &b) const {
    return a == b;
}


Graph::Graph(const Robot &_init, const Board &_board) {
    generateGraph(_init, _board);
}

std::ostream &operator<<(std::ostream &out, const Graph &graph) {
    for (std::pair<Robot, std::vector<std::pair<Robot::Move, Robot>>> e: graph.graph) {
        out << e.first << ":\n";
        for (std::pair<Robot::Move, Robot> p: e.second) {
            out << "\t" << p.first << ": " << p.second << "\n";
        }
    }
    return out;
}

void Graph::generateGraph(const Robot &_init, const Board &_board) {
    std::cout << "Starting Graph gen" << std::endl;
    //int counter = 0; //debug only
    std::queue<Robot> evalQueue;
    evalQueue.push(_init);

    auto chrono_start = std::chrono::high_resolution_clock::now();


    while (!evalQueue.empty()) {
        Robot front = evalQueue.front();
        evalQueue.pop();
        if (graph.find(front) == graph.end()) { // means its not found in graph
            std::vector<std::pair<Robot::Move, Robot>> all_Arcs;

            for (const auto &move : moves) {
                Robot instance = front;
                _board.play(instance, move);
                if (instance.status != Robot::Status::DEAD) {
                    all_Arcs.emplace_back(move, instance);
                    evalQueue.push(instance);
                }
            }
            graph[front] = all_Arcs;
        }
    }

    auto chrono_end = std::chrono::high_resolution_clock::now();
    auto ms = std::chrono::duration<double, std::ratio<1, 1000>>(chrono_end - chrono_start);
    std::cout << "Graph successfully generated in : " << ms.count() << "ms, size is " << graph.size()<<" arcs." << std::endl;
}

std::vector<std::pair<Robot::Move, Robot>> Graph::findPath(const Robot &_from, const Location &_to) {
    std::vector<std::pair<Robot::Move, Robot>> moveOrder;
    std::queue<Robot> unvisitedNodes;
    std::unordered_set<Robot, RobotHash, RobotHash> visitedNodes;


    std::unordered_map<Robot, std::pair<Robot::Move, Robot>, RobotHash, RobotHash> predictedPath;

    auto chrono_start = std::chrono::high_resolution_clock::now();

    for (const auto &obj : graph) costMap[obj.first] = std::numeric_limits<uint64_t>::max();
    costMap[_from] = 0;
    unvisitedNodes.push(_from);


    while (!unvisitedNodes.empty()) {
        Robot current = unvisitedNodes.front();
        unvisitedNodes.pop();
        if (visitedNodes.find(current) != visitedNodes.end()) continue;
        else {
            visitedNodes.insert(current);
            for (std::pair<Robot::Move, Robot> arc : graph[current]) {
                Robot next = arc.second;
                if (costMap[next] > costMap[current] + 1) {
                    costMap[next] = costMap[current] + 1;
                    predictedPath.emplace(next, std::make_pair(arc.first, current));
                    unvisitedNodes.push(next);
                }
            }
        }
    }


    Robot currentDir(_to, Robot::Status::NORTH);

    for (const auto &_status : allStatus) {
        Robot instance(_to, _status);
        if (costMap[instance] < costMap[currentDir]) {
            currentDir = instance;
        }
    }
    if (costMap[currentDir] == std::numeric_limits<uint64_t>::max()) return {};

    while (currentDir != _from) {
        std::pair<Robot::Move, Robot> move = predictedPath.find(currentDir)->second;
        moveOrder.push_back(move);
        currentDir = move.second;
    }

    auto chrono_end = std::chrono::high_resolution_clock::now();
    auto ms = std::chrono::duration<double, std::ratio<1, 1000>>(chrono_end - chrono_start);
    /* debug *//*
    std::cout<<"costmap size : " <<costMap.size()<<std::endl;
    int ind = 0;
    for(auto truc: costMap){
        std::cout << ind++ <<" w = "<<truc.second<<" ,,, " << truc.first <<std::endl;
    }
    std::cout << "path found in : " << ms.count() << "ms" << std::endl;*/
    moveOrder.shrink_to_fit();
    bestMoves=moveOrder;
    return moveOrder;
}
