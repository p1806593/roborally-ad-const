//
// Created by constantin on 06/04/2021.
//

#ifndef ROBORALLY_DECK_HPP
#define ROBORALLY_DECK_HPP

#include <utility>
#include <iostream>
#include <random>
#include <vector>
#include <list>
#include <array>
#include "board.hpp"
#include "Graph.hpp"


using namespace RR;

class RandomManager {
public:
    RandomManager(const uint8_t & _low=0, const uint8_t & _high=10);
    uint8_t getRandom();
private:
    std::default_random_engine randomEngine;
    std::uniform_int_distribution<u_int8_t> manager;
};

class Card {
public:
    Card() {}

    Card(const Robot::Move & _move) : cardAction(_move){};
    friend std::ostream& operator<<(std::ostream &_out, const Card& _hand){
        return _out << _hand.cardAction;
    }
    Robot::Move getAction() { return cardAction ;};
private:
    Robot::Move cardAction;
};


class Hand {
public:
    Hand(const unsigned int & _handSize=9);
    Hand(const std::vector<Card> & _from){currentHand=_from;}
    void drawHand();
    std::vector<Card> makePath(const Board&, Robot, Graph&,Location & _to);
    std::vector<Card> getHand() const {return currentHand;}
    friend std::ostream& operator<<(std::ostream &_out, const Hand& _hand){
        int index=0;
        _out << "current hand : " << std::endl;
        for(const Card & cardInstance : _hand.currentHand){
            _out <<index++ << " : " << cardInstance <<std::endl;
        }
        return _out;
    }
private:
    RandomManager randomManager;
    std::vector<Card> currentHand;

};


#endif //ROBORALLY_DECK_HPP
