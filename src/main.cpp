//
// Created by constantin on 27/03/2021.
//

#include "Graph.hpp"
#include "Deck.hpp"
#include <thread>

using namespace RR;

int main(int argc, char **argv){
    Board board(argv[1]);
    Robot robot(Location(2,2),Robot::Status::NORTH);

    Graph graph (robot,board);

    Location destination(19,10);

    bool turn = true;
    int turns = 0;
    while(turn && robot.status != Robot::Status::DEAD){
        auto res = graph.findPath(robot,destination);
        Hand myHand;

        std::cout<< std::endl << std::endl << "numbers of move to dest : "<<res.size()<<std::endl;
        for ( auto it = res.rbegin(); it != res.rend(); it++) {
            std::cout <<"from : " << it->second<< " do : " << it->first<<std::endl;
        }
        auto test = myHand.makePath(board,robot,graph,destination);

        for (auto & it : test) {
            std::cout<<it<< std::endl;
            board.play(robot,it.getAction());
        }
        myHand = Hand();
        std::cout<<robot<<std::endl;
        turn= !res.empty();
        //std::this_thread::sleep_for(std::chrono::milliseconds(500));
        ++turns;
    }

    std::cout <<std::endl << "desto reached in " << turns - 1 << " turns :D"<<std::endl;

    return 0;
}